# gSheets ToDo

## Eine ToDo Liste die ein Google Spreadsheet als Speicher verwendet

[**Diese Dokumentation ist am besten auf GitLab.com zu betrachten.**](https://gitlab.com/casaper/gSheetsToDo/blob/master/README.md)

# Inhaltsverzeichnis

- [Eine ToDo Liste die ein Google Spreadsheet als Speicher verwendet](#eine-todo-liste-die-ein-google-spreadsheet-als-speicher-verwendet)
- [Grundlegende Funktionsweise](#grundlegende-funktionsweise)
- [Features](#features)
- [Activity Diagramme](#activity-diagramme)
  - [Acitivty: Autorisierung zu Google Sheets API](#acitivty-autorisierung-zu-google-sheets-api)
  - [Mark task Done](#mark-task-done)
- [Assoziationen](#assoziationen)
  - [Activities](#activities)
  - [Task Abbildung](#task-abbildung)
- [Sheet Zur Speicherung](#sheet-zur-speicherung)
- [Story Board](#story-board)
  - [Vorgang: Der App den Zugang zur Google Sheets API zu erlauben](#vorgang-der-app-den-zugang-zur-google-sheets-api-zu-erlauben)
  - [Task Hinzufügen](#task-hinzufügen)
  - [Task gespeichert](#task-gespeichert)
  - [Task erledigt markieren](#task-erledigt-markieren)
  - [Task Löschen](#task-löschen)
  - [Options Menu](#options-menu)

## Grundlegende Funktionsweise

Geplant ist eine Android ToDo Liste, die ihre Daten nicht im Gerät, sondern in einem Spreadsheet von Google Sheets in Google Drive speichert.

Dies hat den Vorteil, das die Taskliste einerseits hübsch dargestellt im Handy verfügbar ist, andererseits aber bequem und übersichtlich als Spreadsheet im Desktopcomputer bearbeitet werden kann.

Hierfür will ich die Google Spreadsheets API verwenden.

## Features

- Mit Google Spreadsheet verbunden
- Task mit Titel und Beschreibung
- Task kann als erledigt markiert werden
- Task kann als unerledigt markiert werden
- Task kann gelöscht werden

## Activity Diagramme

### Acitivty: Autorisierung zu Google Sheets API

Activity Diagramm zur Authorisierung der App für die Google Sheets API des Benutzers:

![Activity: Account Authorisations Vorgang](docu/ActivityGoogleApiAuthorization.png)

### Mark task Done

Task-Item als erledigt markieren – Die Speicherung in der Sheets API:

![Activity: Task item als erledigt markieren](docu/SaveTaskChecked.png)

## Assoziationen

### Activities

Android Activity Verbindungen:

![Activities Assoziationen](docu/ActivityAssotiationsDiagram.png)

### Task Abbildung

Speicherung der Tasks erfolgt in einem Online Google Sheet. Hierzu lädt sich die App bei der Initialisierung die Tasks des Sheets herunter und bildet die Dann in ihrer lokalen Struktur ab.

![Task Storage Associations](docu/TaskStorage.png)

## Sheet Zur Speicherung

Hier ist eine erste Vorlage eines Speichersheets auf Google: [**Task List Spread Sheet**](https://docs.google.com/spreadsheets/d/1p_ea3Ju-c19-zzbkmC6mnno2VUzs3iB4RRP3UCP3xtE/edit?usp=sharing)

## Story Board

Hier Habe ich mittels dem [Tool Marvel](https://marvelapp.com/) ein Mockup als Storyboard der App erstellt.

[**StoryBoard Mockup**](https://marvelapp.com/7h8heh4)

[![Mockup QR-Code für das Handy](docu/mockup_qr_code.png)](https://marvelapp.com/7h8heh4)

### Vorgang: Der App den Zugang zur Google Sheets API zu erlauben

1. Schritt

![Kontakt zugriff erlauben](docu/marvel_screens/2018-02-06-11-36-40.png)

2. Schritt

![Account auswählen](docu/marvel_screens/2018-02-06-11-37-13.png)

3. Schritt

![Gsheets app authorisieren](docu/marvel_screens/2018-02-06-11-37-34.png)

### Task Hinzufügen

![Neuen Task Erstellen](docu/marvel_screens/2018-02-06-13-11-38.png)

### Task gespeichert

![Task gespeichert](docu/marvel_screens/2018-02-06-13-12-01.png)

### Task erledigt markieren

![Tasks alle nicht erledigt](docu/marvel_screens/2018-02-06-13-13-57.png)

![Task erledigt markiert](docu/marvel_screens/2018-02-06-13-14-16.png)

### Task Löschen

![Task noch nicht gelöscht](docu/marvel_screens/2018-02-06-13-13-57.png)

![Rüber swipen um zu löschen](docu/marvel_screens/2018-02-06-13-14-54.png)

![Eimer getippt, task ist weg](docu/marvel_screens/2018-02-06-13-15-24.png)

### Options Menu

![Das Optionsmenu wird mit dem Hamburger aktiviert](docu/marvel_screens/2018-02-06-13-16-06.png)
