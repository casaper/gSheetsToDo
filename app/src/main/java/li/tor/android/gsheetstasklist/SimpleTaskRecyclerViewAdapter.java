package li.tor.android.gsheetstasklist;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;

import li.tor.android.gsheetstasklist.tasks.local.LocalTask;
import li.tor.android.gsheetstasklist.tasks.local.LocalTaskRepository;

/**
 * SimpleTaskRecyclerViewAdapter
 */
public class SimpleTaskRecyclerViewAdapter
        extends RecyclerView.Adapter<SimpleTaskRecyclerViewAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    /**
     * Property for availability within click listener callback
     */
    private LocalTaskRepository localTaskRepository;

    /**
     * localTaskRepository getter, to be used within the nested click listener callbacks
     * @return taskRepository
     */
    public LocalTaskRepository getLocalTaskRepository() { return localTaskRepository; }

    /**
     * Self getter, in order to have the adapter available in nested click handler callbacks
     *
     * @return the adapter
     */
    public SimpleTaskRecyclerViewAdapter getAdapter() { return this; }


    /**
     * Click listener for marking a task item done or undone
     */
    private final View.OnClickListener toggleDoneClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            LocalTask item = (LocalTask) view.getTag();
            item.toggleDone();
            getLocalTaskRepository().updateTask(item);
            FloatingActionButton clickedButton = (FloatingActionButton)view;
            clickedButton.setVisibility(View.INVISIBLE);
            getAdapter().notifyItemChanged(item.position, item);
        }
    };

    /**
     * Click listener for editing a task item
     */
    private final View.OnClickListener editTaskClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            LocalTask item = (LocalTask) view.getTag();
            Context context = view.getContext();
            Intent intent = new Intent(context, TaskEditActivity.class);
            intent.putExtra(TaskEditFragment.ARG_TASK_OBJ, item);
            context.startActivity(intent);
        }
    };

    /**
     * Constructor for the List adapter
     *
     * @param repository of my tasks
     */
    SimpleTaskRecyclerViewAdapter(LocalTaskRepository repository) {
        this.localTaskRepository = repository;
    }

    /**
     * Bind task list item layout to the recycler list
     *
     * @param parent
     * @param viewType
     * @return Task list Item view holder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.task_list_content, parent, false);
        return new ViewHolder(view);
    }

    /**
     * Initialize the task list item
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LocalTask currentTask = localTaskRepository.getPosition(position);
        // Stop if task not found
        if (currentTask == null) return;

        // Set task toolbar title
        holder.titleTextView.setText(currentTask.getTitle());
        // Set Title and  Description text
        holder.descriptionTextView.setText(currentTask.getDescription());

        // Get elements
        TextView itemTitle = holder.itemView.findViewById(R.id.title);
        TextView itemDescription = holder.itemView.findViewById(R.id.description);
        FloatingActionButton notDoneButton = holder.itemView.findViewById(R.id.btn_task_not_done);
        FloatingActionButton doneButton = holder.itemView.findViewById(R.id.btn_task_is_done);

        // attach the task item to elements, so they can use it in their event callbacks
        itemTitle.setTag(currentTask);
        itemDescription.setTag(currentTask);
        notDoneButton.setTag(currentTask);
        doneButton.setTag(currentTask);

        // Make the right done button visible and add the toggle done listener to it
        if(currentTask.isDone()) {
            doneButton.setVisibility(View.VISIBLE);
            doneButton.setOnClickListener(toggleDoneClickListener);
            doneButton.setTag(currentTask);
        } else {
            notDoneButton.setVisibility(View.VISIBLE);
            notDoneButton.setOnClickListener(toggleDoneClickListener);
            notDoneButton.setTag(currentTask);
        }

        // Add the edit click listener to the title and description view
        itemTitle.setOnClickListener(editTaskClickListener);
        itemDescription.setOnClickListener(editTaskClickListener);
    }

    /**
     * Neccesity for the list adapter
     *
     * @return number of tasks in repository
     */
    @Override
    public int getItemCount() {
        return localTaskRepository.size();
    }

    /**
     * Called when an item has been dragged far enough to trigger a move. This is called every time
     * an item is shifted, and <strong>not</strong> at the end of a "drop" event.<br/>
     * <br/>
     * Implementations should call {@link RecyclerView.Adapter#notifyItemMoved(int, int)} after
     * adjusting the underlying data to reflect this move.
     *
     * @param fromPosition The start position of the moved item.
     * @param toPosition   Then resolved position of the moved item.
     * @see RecyclerView#getAdapterPositionFor(RecyclerView.ViewHolder)
     * @see RecyclerView.ViewHolder#getAdapterPosition()
     */
    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    /**
     * Called when an item has been dismissed by a swipe.<br/>
     * <br/>
     * Implementations should call {@link RecyclerView.Adapter#notifyItemRemoved(int)} after
     * adjusting the underlying data to reflect this removal.
     *
     * @param position The position of the item dismissed.
     * @see RecyclerView#getAdapterPositionFor(RecyclerView.ViewHolder)
     * @see RecyclerView.ViewHolder#getAdapterPosition()
     */
    @Override
    public void onItemDismiss(int position) {

    }

    /**
     * Binds the task item view elements to a single entity
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView titleTextView;
        final TextView descriptionTextView;

        ViewHolder(View view) {
            super(view);
            titleTextView = view.findViewById(R.id.title);
            descriptionTextView = view.findViewById(R.id.description);
        }
    }
}