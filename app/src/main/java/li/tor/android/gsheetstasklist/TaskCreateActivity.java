package li.tor.android.gsheetstasklist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import li.tor.android.gsheetstasklist.tasks.local.LocalTask;

import static li.tor.android.gsheetstasklist.R.id.task_desription_text_create;
import static li.tor.android.gsheetstasklist.R.id.task_title_text_create;
import static li.tor.android.gsheetstasklist.R.layout.activity_task_create;

/**
 * An activity for creating new tasks.
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class TaskCreateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_task_create);
        setSupportActionBar(findViewById(R.id.toolbar));

        // Add click event listener to the save button
        FloatingActionButton saveNewTaskButton = findViewById(R.id.btn_save_new_task);
        saveNewTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getSelf(), TaskListActivity.class);
                // Fetch the Title string from form field
                String title = ((EditText)findViewById(task_title_text_create)).getText().toString();

                /**
                 * Catch empty title field. Task should not be saved in that case.
                 */
                if (title.isEmpty()) {
                    // Set Error message to be displayed by parameter
                    intent.putExtra(TaskListActivity.ARG_TITLE_EMPTY_ERROR, true);
                    NavUtils.navigateUpTo(getSelf(), intent);
                } else {
                    String description = ((EditText) findViewById(task_desription_text_create)).getText().toString();
                    LocalTask task = new LocalTask(title, description);
                    // Add the new Task as parameter to intent
                    intent.putExtra(TaskListActivity.ARG_CREATE_TASK_OBJ, task);
                    NavUtils.navigateUpTo(getSelf(), intent);
                }
            }
        });
    }

    public TaskCreateActivity getSelf() { return this; }

}
