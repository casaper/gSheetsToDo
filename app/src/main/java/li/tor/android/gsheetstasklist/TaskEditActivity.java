package li.tor.android.gsheetstasklist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import li.tor.android.gsheetstasklist.tasks.local.LocalTask;

import static li.tor.android.gsheetstasklist.R.id.btn_save_task;
import static li.tor.android.gsheetstasklist.R.id.task_desription_text_edit;
import static li.tor.android.gsheetstasklist.R.id.task_title_text_edit;

/**
 * An activity for editing existing Tasks.
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class TaskEditActivity extends AppCompatActivity {
    public LocalTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton saveTaskButton = findViewById(btn_save_task);
        saveTaskButton.setOnClickListener(saveOnClickListener);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putSerializable(
                    TaskEditFragment.ARG_TASK_OBJ,
                    getIntent().getSerializableExtra(TaskEditFragment.ARG_TASK_OBJ)
            );
            this.task = (LocalTask) getIntent().getSerializableExtra(TaskEditFragment.ARG_TASK_OBJ);
            TaskEditFragment fragment = new TaskEditFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.task_edit_container, fragment)
                    .commit();
        }
    }

    /**
     * Setter for task field
     *
     * @param task the edit subject
     */
    public void setTask(LocalTask task) {
        this.task = task;
    }

    /**
     * Task getter to be used within a on Click handler
     * anonymous function
     *
     * @return
     */
    public LocalTask getTask() {
        return this.task;
    }

    /**
     * Instance getter to be used within a on Click handler
     * anonymous function
     *
     * @return activity - this this instance
     */
    public TaskEditActivity getSelf() {
        return this;
    }

    /**
     * Listener for Save Task action button
     */
    private final View.OnClickListener saveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            LocalTask task = getTask();
            Intent intent = new Intent(getSelf(), TaskListActivity.class);
            String title = ((EditText) findViewById(task_title_text_edit)).getText().toString();
            if (title.isEmpty()) {
                intent.putExtra(TaskListActivity.ARG_TITLE_EMPTY_ERROR, true);
                NavUtils.navigateUpTo(getSelf(), intent);
            } else {
                task.setTitle(title);
                task.setDescription(
                        ((EditText) findViewById(task_desription_text_edit)).getText().toString()
                );
                task.setDone(
                        ((CheckBox) findViewById(R.id.done_check_box)).isChecked()
                );
                intent.putExtra(TaskListActivity.ARG_UPDATE_TASK_OBJ, task);
                NavUtils.navigateUpTo(getSelf(), intent);
            }
        }
    };
}
