package li.tor.android.gsheetstasklist;

import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import li.tor.android.gsheetstasklist.tasks.local.LocalTask;

// Import the view resources statically
import static li.tor.android.gsheetstasklist.R.id.task_desription_text_edit;
import static li.tor.android.gsheetstasklist.R.id.task_title_text_edit;
import static li.tor.android.gsheetstasklist.R.id.toolbar_layout;

/**
 * A fragment with input fields for a single Task.
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class TaskEditFragment extends Fragment {
    /**
     * The fragment argument representing the serialized LocalTask instance
     */
    public static final String ARG_TASK_OBJ = "task_obj";

    /**
     * The LocalTask instance from serialized parameter
     */
    private LocalTask task;

    /**
     * Parent activity
     */
    private Activity activity;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TaskEditFragment() {
    }

    /**
     * Instantiates the Fragment with the LocalTask as serialized parameter
     *
     * @param clickedTask - clicked in the task list
     * @return this fragment
     */
    public static TaskEditFragment newInstance(LocalTask clickedTask) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_TASK_OBJ, clickedTask);

        TaskEditFragment fragment = new TaskEditFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    /**
     * Extracts the serialized Task object from the bundle
     *
     * @param bundle
     */
    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            task = (LocalTask) bundle.getSerializable(ARG_TASK_OBJ);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this.getActivity();
        if (getArguments().containsKey(ARG_TASK_OBJ)) {
            readBundle(getArguments());
            CollapsingToolbarLayout appBarLayout = activity.findViewById(toolbar_layout);
        }
    }

    /**
     * Attaches the Tasks properties to the edit view fields
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.task_edit, container, false);

        // Set the existing text to the edit fields
        if (task != null) {
            ((EditText) rootView.findViewById(task_title_text_edit))
                                .setText(task.getTitle());
            ((EditText) rootView.findViewById(task_desription_text_edit))
                                .setText(task.getDescription());
            ((CheckBox) rootView.findViewById(R.id.done_check_box)).setChecked(task.isDone());
        }

        return rootView;
    }
}
