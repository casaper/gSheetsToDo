package li.tor.android.gsheetstasklist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import li.tor.android.gsheetstasklist.tasks.google.GoogleToDoSheet;
import li.tor.android.gsheetstasklist.tasks.local.LocalTask;
import li.tor.android.gsheetstasklist.tasks.local.LocalTaskRepository;

import static li.tor.android.gsheetstasklist.R.id.btn_initiate_create_task;
import static li.tor.android.gsheetstasklist.R.id.task_list;
import static li.tor.android.gsheetstasklist.R.id.task_list_frame_layout;
import static li.tor.android.gsheetstasklist.R.layout.activity_task_list;

/**
 * An activity representing a list of Tasks.
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class TaskListActivity extends AppCompatActivity {

    private GoogleToDoSheet gSheet;
    private LocalTaskRepository localTaskRepository;
    public static final String ARG_UPDATE_TASK_OBJ = "task_obj";
    public static final String ARG_CREATE_TASK_OBJ = "new_task_obj";
    public static final String ARG_TITLE_EMPTY_ERROR = "title_was_empty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Initialize Things
         */
        initializeProperties();
        setupActivitiesLayout();
        handleCreateOrUpdate();
        setUpCreateTaskButton();

        /**
         * Setup the Recycler view for the items
         */
        View recyclerView = findViewById(task_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
    }

    /**
     * Create new Task button initialization with click listener
     */
    private void setUpCreateTaskButton() {
        FloatingActionButton fab = findViewById(btn_initiate_create_task);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, TaskCreateActivity.class);
                context.startActivity(intent);
            }
        });
    }

    /**
     * Initialize the layout and bind its properties
     */
    private void setupActivitiesLayout() {
        setContentView(activity_task_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    /**
     * Initialize the classes properties
     */
    private void initializeProperties() {
        if (gSheet == null) {
            gSheet = GoogleToDoSheet.getInstance();
        }
        if (localTaskRepository == null) {
            localTaskRepository = LocalTaskRepository.getInstance(gSheet);
            if (localTaskRepository.size() < 1) {
                if (localTaskRepository.size() < 1) setupDummyTasksForDevelopment();
            }
        }
    }

    /**
     * Handles parameters other activities pass on navigating back
     */
    private void handleCreateOrUpdate() {
        if(getIntent().getBooleanExtra(ARG_TITLE_EMPTY_ERROR, false)) {
            Snackbar.make(findViewById(task_list_frame_layout), "Task with empty Title not saved!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        LocalTask createdNewTask = (LocalTask) getIntent().getSerializableExtra(ARG_CREATE_TASK_OBJ);
        if (createdNewTask != null) {
            localTaskRepository.addTask(createdNewTask);
            return;
        }

        LocalTask updatedTask = (LocalTask) getIntent().getSerializableExtra(ARG_UPDATE_TASK_OBJ);
        if (updatedTask != null) {
            localTaskRepository.updateTask(updatedTask);
        }
    }

    /**
     * Method that sets up dummy tasks while developping
     * TODO: Remove after development
     */
    private void setupDummyTasksForDevelopment() {
        for (int i = 1; i < 5; i++) {
            LocalTask newTask = new LocalTask("Task Title Nummer " + i, "Task description number " + i);
            newTask.setId(i);
            localTaskRepository.addTask(newTask);
        }
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleTaskRecyclerViewAdapter(localTaskRepository));
    }
}
