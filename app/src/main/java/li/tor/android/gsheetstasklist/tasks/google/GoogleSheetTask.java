package li.tor.android.gsheetstasklist.tasks.google;

import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;

import java.util.Date;

/**
 * Local representation of a single task item in the
 * Google Spreadsheet
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class GoogleSheetTask {
    private Integer id;
    private boolean done;
    private String title;
    private String description;
    private String created_at;
    private String updated_at;
    private String marked_done_at;
    private String deleted_at;

    /**
     * Constructor for new Task
     * @param done
     * @param title
     * @param description
     */
    public GoogleSheetTask(boolean done, String title, String description, Date created_at, Date updated_at) {
        this.done = done;
        this.title = title;
        this.description = description;
        this.created_at = createTimeString(created_at);
        this.updated_at = createTimeString(updated_at);
    }

    /**
     * Constructor for existing task
     * @param done
     * @param title
     * @param description
     */
    public GoogleSheetTask(
        Integer id, boolean done, String title, String description, Date created_at,
        Date updated_at, Date marked_done_at, Date deleted_at
    ) {
        this.done = done;
        this.title = title;
        this.description = description;
        this.created_at = createTimeString(created_at);
        this.updated_at = createTimeString(updated_at);
    }

    /**
     * Checks if this task has this id
     *
     * @param id
     * @return is or is not
     */
    public boolean isId(Integer id) {
        return this.id == id;
    }

    /**
     * ISO Time string from date
     * for the date fields in sheet
     * 
     * @param date
     * @return
     */
    public String createTimeString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return df.format(date.getTime());
    }
}
