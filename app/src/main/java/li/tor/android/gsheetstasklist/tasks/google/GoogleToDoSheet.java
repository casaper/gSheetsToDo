package li.tor.android.gsheetstasklist.tasks.google;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import li.tor.android.gsheetstasklist.tasks.local.LocalTask;

/**
 * Local Representation of the Google spreadsheet
 * holding the Tasks
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class GoogleToDoSheet {
    private List<GoogleSheetTask> sheetTasks;
    /**
     * Singleton
     */
    private static GoogleToDoSheet ourInstance;
    synchronized public static GoogleToDoSheet getInstance() {
        if (ourInstance == null) ourInstance = new GoogleToDoSheet();
        return ourInstance;
    }

    /**
     * Constructor
     */
    public GoogleToDoSheet() {
        if (sheetTasks == null) sheetTasks = new ArrayList<>();
    }

    public boolean addTask(LocalTask task) {
        GoogleSheetTask newTask = new GoogleSheetTask(
            task.getId(), task.isDone(), task.getTitle(), task.getDescription(),
            task.getCreatedAt(), task.getUpdatedAt(), task.getMarkedDoneAt(), task.getDeletedAt()
        );
        if (newTask == null) {
            return false;
        }
        sheetTasks.add(newTask);
        return true;
    }

    public GoogleSheetTask getId(int id) {
        Optional<GoogleSheetTask> taskFound = sheetTasks
                .stream()
                .filter(item -> item.isId(id))
                .findFirst();

        if (!taskFound.isPresent()) return null;
        return taskFound.get();
    }

    public boolean updateTask(LocalTask changed) {
        sheetTasks.remove(getId(changed.getId()));
        return addTask(changed);
    }


}
