package li.tor.android.gsheetstasklist.tasks.local;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Tasks model for device side representation.
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class LocalTask implements Serializable {
    private Integer id;
    private boolean done;
    private String title;
    private String description;
    private Date created_at;
    private Date updated_at;
    private Date marked_done_at;
    private Date deleted_at;
    public Integer position;

    /**
     * Constructor from Google Spreadsheet
     *
     * @param id
     * @param done
     * @param title
     * @param description
     * @param created_at
     * @param updated_at
     * @param marked_done_at
     * @param deleted_at
     */
    public LocalTask(
            Integer id, boolean done, String title, String description, Date created_at,
            Date updated_at, Date marked_done_at, Date deleted_at
    ) {
        this.id = id;
        this.done = done;
        this.title = title;
        this.description = description;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.marked_done_at = marked_done_at;
        this.deleted_at = deleted_at;
    }

    /**
     * Constructor for new Task
     *
     * @param title
     * @param description
     */
    public LocalTask(String title, String description) {
        this.id = null;
        this.done = false;
        this.title = title;
        this.description = description;
        this.created_at = getNow();
        this.updated_at = getNow();
        this.marked_done_at = null;
        this.deleted_at = null;
    }

    /**
     * get the current DateTime
     *
     * @return now
     */
    private Date getNow() {
        Calendar rightNow = Calendar.getInstance();
        return rightNow.getTime();
    }

    /**
     * Toggle Done and Undone
     *
     * @return new done state
     */
    public boolean toggleDone() {
        this.done = !this.done;
        this.updated_at = getNow();
        this.marked_done_at = getNow();
        return this.done;
    }

    /**
     * Set a tasks done state with boolean
     *
     * @param state
     * @return the new state of the task
     */
    public boolean setDone(boolean state) {
        done = state;
        return done;
    }

    /**
     * Get done State
     *
     * @return done state
     */
    public boolean isDone() {
        return done;
    }

    /**
     * Getter for Title
     *
     * @return the title of the task
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter for Title
     *
     * @param title the title of the task
     */
    public void setTitle(String title) {
        this.title = title;
        updated_at = getNow();
    }

    /**
     * Getter for Description
     *
     * @return the description of the Task
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for Description
     *
     * @param description - the description of the task
     */
    public void setDescription(String description) {
        this.description = description;
        updated_at = getNow();
    }

    /**
     * Copy the new values from a serialized LocalTask object to
     * its existing counterpart
     *
     * @param changed
     * @return the updated task
     */
    public LocalTask updateFromChanged(LocalTask changed) {
        title = changed.getTitle();
        description = changed.getDescription();
        done = changed.isDone();
        updated_at = changed.getUpdatedAt();
        deleted_at = changed.getDeletedAt();
        marked_done_at = changed.getMarkedDoneAt();
        position = changed.position;
        return this;
    }

    /**
     * Getter for CreatedAt
     *
     * @return created record Date
     */
    public Date getCreatedAt() {
        return created_at;
    }

    /**
     * Getter for UpdatedAt
     *
     * @return updated record Date
     */
    public Date getUpdatedAt() {
        return updated_at;
    }

    /**
     * Getter for MarkedDonedAt
     *
     * @return marked_done_at record Date
     */
    public Date getMarkedDoneAt() {
        return marked_done_at;
    }

    public boolean isId(Integer id) {
        return this.id == id;
    }

    /**
     * Getter for DeletedAt
     *
     * @return deleted record Date
     */
    public Date getDeletedAt() {
        return deleted_at;
    }

    /**
     * Getter for id
     *
     * @return the tasks id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter for id
     *
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Mark Task deleted, by setting its deleted_at Date
     *
     * @return the date it was marked deleted at
     */
    public Date setDeleted() {
        Date modificationDate = getNow();
        deleted_at = modificationDate;
        updated_at = modificationDate;
        return modificationDate;
    }
}
