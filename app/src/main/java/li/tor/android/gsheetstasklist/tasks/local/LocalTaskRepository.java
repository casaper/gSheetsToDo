package li.tor.android.gsheetstasklist.tasks.local;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import li.tor.android.gsheetstasklist.tasks.google.GoogleToDoSheet;

/**
 * Takes care of local task list storage
 *
 * @author Kaspar Vollenweider
 * @date 2018-02-09
 */
public class LocalTaskRepository {

    // Tasks list
    private List<LocalTask> tasks;

    // The Sheet, in order to synchronise local tasks with the sheet
    private GoogleToDoSheet gSheet;

    /**
     * Singleton pattern, to ensure only one instance exists
     */
    private static LocalTaskRepository ourInstance;
    synchronized public static LocalTaskRepository getInstance(GoogleToDoSheet gSheet) {
        if(ourInstance == null) ourInstance = new LocalTaskRepository(gSheet);
        return ourInstance;
    }

    /**
     * Constructor
     */
    public LocalTaskRepository(GoogleToDoSheet gSheet) {
        if (tasks == null) this.tasks = new ArrayList<>();
        if (this.gSheet == null) this.gSheet = gSheet;
    }

    public List<LocalTask> getAllTasks() {
        return new ArrayList<>(tasks);
    }

    /**
     * Load All The tasks from gSheet
     *
     * @return load success
     */
    public boolean loadTasksFromSheet() {
        // TODO: Sheet loader
        return false;
    }

    /**
     * Get a Task by its id
     *
     * @param id
     * @return the task
     */
    public LocalTask getById(int id) {
        Optional<LocalTask> taskFound = getNotRemoved()
                .stream()
                .filter(item -> item.getId() == id)
                .findFirst();

        if (!taskFound.isPresent()) return null;
        return taskFound.get();
    }

    /**
     * Get item by position in list
     *
     * @param position
     * @return
     */
    public LocalTask getPosition(int position) {
        Optional<LocalTask> taskFound = getNotRemoved()
                .stream()
                .filter(item -> item.isId(position))
                .findFirst();
        if (taskFound.isPresent()) {
            LocalTask found = taskFound.get();
            found.position = position;
            return found;
        } else {
            LocalTask idTask = getNotRemovedAndNoPosition().get(0);
            idTask.position = position;
            return idTask;
        }
    }

    /**
     * Add New Task
     *
     * @param task
     * @return save to Sheet success
     */
    public boolean addTask(LocalTask task) {
        task.setId(nextId());
        tasks.add(task);
        return gSheet.addTask(task);
    }

    /**
     * Generate incremented id
     *
     * @return the next id
     */
    private Integer nextId() {
        if (this.size() < 1) {
            return 1;
        }
        return getAllIds().get(this.size() - 1) + 1;
    }

    /**
     * Mark Task deleted (virtually remove it)
     *
     * @param toRemove task object to be marked deleted
     * @return date deleted_at is set
     */
    public Date removeTask(LocalTask toRemove) {
        LocalTask current = getById(toRemove.getId());
        return current.setDeleted();
    }

    /**
     * Toggle the done property to the oposite of the current state
     *
     * @param toToggle task object that needs to be toggled in the list
     * @return done state of the task
     */
    public boolean toggleTaskDone(LocalTask toToggle) {
        LocalTask current = this.getById(toToggle.getId());
        current.toggleDone();
        return current.isDone();
    }

    /**
     * Get a list with all the tasks ids, ordered by their ids
     *
     * @return ids of all tasks
     */
    public List<Integer> getAllIds() {
        return tasks.stream()
                .map(task -> task.getId())
                .sorted((a, b) -> Integer.compare(a, b))
                .collect(Collectors.toList());
    }

    /**
     * Update Task with LocalTask instance
     *
     * @param changed
     * @return success state
     */
    public boolean updateTask(LocalTask changed) {
        LocalTask oldTask = this.getById(changed.getId());
        if(oldTask.updateFromChanged(changed) != null) {
            return true;
        }
        return false;
    }

    /**
     * Get all tasks with no position
     *
     * @return list of removed tasks
     */
    public List<LocalTask> getNoPosition() {
        return tasks.stream()
                .filter(task -> task.position == null)
                .collect(Collectors.toList());
    }

    /**
     * Get all removed tasks
     *
     * @return list of removed tasks
     */
    public List<LocalTask> getRemoved() {
        return tasks.stream()
                .filter(task -> task.getDeletedAt() != null)
                .collect(Collectors.toList());
    }

    /**
     * Get all not removed tasks
     *
     * @return list of removed tasks
     */
    public List<LocalTask> getNotRemoved() {
        return tasks.stream()
                .filter(task -> task.getDeletedAt() == null)
                .collect(Collectors.toList());
    }

    /**
     * Get all not removed tasks
     *
     * @return list of removed tasks
     */
    public List<LocalTask> getNotRemovedAndNoPosition() {
        return tasks.stream()
                .filter(task -> (task.getDeletedAt() == null && task.position == null))
                .collect(Collectors.toList());
    }


    /**
     * Get the count of Tasks in the list
     *
     * @return the amount of Tasks
     */
    public Integer size() {
        return getNotRemoved().size();
    }

}
