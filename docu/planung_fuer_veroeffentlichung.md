# Planung für die Veröffentlichung der App

## Tesprotokoll

Mein Testprotokoll habe ich mittels eines Spreadsheets erstellt.

[Dieses ist hier online einsehbar](https://goo.gl/kZq63s).

## Anforderungen von Google für die Aufnahme im Google PlayStore



1. Ein Google [Developer Account](https://support.google.com/googleplay/android-developer/answer/6112435)
1. [Quality guidelines](https://developer.android.com/develop/quality-guidelines/index.html) von Google müssen eingehalten werden
1. Ein "*release-ready APK*"
1. PlayStore Applikationsbeschreibung [planen](https://developer.android.com/distribute/best-practices/launch/store-listing.html)
1. APK in den Alpha- oder Beta-Kanal [uploaden](https://support.google.com/googleplay/android-developer/answer/113469)
1. [Aplikations Gerätekompatibilität definieren](https://support.google.com/googleplay/android-developer/answer/1286017)
1. Den Preis und die Länder in denen die App verteilt werden [definieren](https://support.google.com/googleplay/android-developer/answer/6334373)
1. [App rating ermitteln](https://support.google.com/googleplay/android-developer/answer/188189)
1. Letzte Checks und dann die App [publizieren](https://support.google.com/googleplay/android-developer/answer/6334282)



